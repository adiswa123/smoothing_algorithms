# Advanced Algorithms project -- Smoothing algorithms

This repository contains an extensive analysis on the various smoothing algorithms used currently.
It also contains python implementations of many of the algorithms

The team consists of -

* Adishwar Rishi
* Aman Singh

In order to run the python code you will need the following - 

1. [Python 3](https://www.python.org/)
2. [NumPy](http://www.numpy.org/)
3. [SciPy](https://www.scipy.org/)